package com.example.androidchatclient.ui.main;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.androidchatclient.R;
import com.example.myapplication.ChatClient;
import com.google.android.material.textfield.TextInputEditText;

import io.grpc.stub.StreamObserver;

public class ChatFragment extends Fragment {

    private ChatViewModel mChatViewModel = new ChatViewModel();

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void linkTextViewChat(final TextView textViewChat)
    {
        final StreamObserver<ChatClient.ChatMessage> streamObserver = new StreamObserver<ChatClient.ChatMessage>() {
            @Override
            public void onNext(final ChatClient.ChatMessage value) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textViewChat.setText(
                                textViewChat.getText().toString() +
                                        "\n" + value.getWhen() + " " + value.getSenderName() + " " + value.getContent());
                        if(textViewChat.getText().length() > 500)
                            textViewChat.setText(value.getWhen() + " " + value.getSenderName() + " " + value.getContent());
                    }
                });
            }

            @Override
            public void onError(Throwable t) {
                textViewChat.setText(t.getMessage());
            }

            @Override
            public void onCompleted() {
                textViewChat.setText("completed");
            }
        };

        mChatViewModel.StartSubscribing(streamObserver);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final TextView textViewChat = (TextView) view.findViewById(R.id.textview_chat);
        linkTextViewChat(textViewChat);

        final TextInputEditText textInputEditText = (TextInputEditText) view.findViewById(R.id.text_input_edit);
        view.findViewById(R.id.button_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!textInputEditText.getText().toString().isEmpty())
                {
                    mChatViewModel.WriteMessage(textInputEditText.getText().toString());
                    textInputEditText.setText("");
                }
            }
        });

    }
}
