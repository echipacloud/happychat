package com.example.androidchatclient.ui.main;

import android.os.AsyncTask;

import com.example.myapplication.ChatClient;
import com.example.myapplication.ChatGrpc;
import com.google.protobuf.Empty;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

public class ChatViewModel {

    private final ManagedChannel mChannel = ManagedChannelBuilder.
            forAddress("boaman.ddns.net", 5000).usePlaintext().build();

    private final ChatGrpc.ChatStub mAsyncStub = ChatGrpc.newStub(mChannel);
    private final ChatGrpc.ChatBlockingStub mBlockingStub = ChatGrpc.newBlockingStub(mChannel);

    public void StartSubscribing(final StreamObserver<ChatClient.ChatMessage> streamObserver) {
        StreamObserver<Empty>  streamChat = mAsyncStub.subscribe(streamObserver);
    }

    public void WriteMessage(String content) {

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        String when = formatter.format(new Date());

        SendChatMessageTask sendChatMessageTask = new SendChatMessageTask();
        AsyncTask<String, Void, Void> result = sendChatMessageTask.execute(content, "AndroidUser", when);
    }

    private  class SendChatMessageTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {

            mBlockingStub.send(
                    ChatClient.ChatMessage.newBuilder()
                            .setContent(params[0]).setSenderName(params[1]).setWhen(params[2]).build()
            );

            return null;
        }
    }
}
