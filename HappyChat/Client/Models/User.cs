﻿using Common.Protos;
using Grpc.Core;
using Grpc.Net.Client;
using Prism.Mvvm;
using System;
using System.Linq;
using System.Reactive.Linq;
using static Common.Protos.Chat;

namespace Client.Models
{
    class User : BindableBase
    {
        private string m_name = "anonim";
        private ChatClient m_client;
        public string Name
        {
            get { return m_name; }
            set { SetProperty(ref m_name, value); }
        }

        public User()
        {
            InitClient();
        }
        public User(String name)
        {
            InitClient();
            m_name = name;
        }

        private void InitClient()
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            string connectionAdress = "http://boaman.ddns.net:5000";

            GrpcChannel channel = GrpcChannel.ForAddress(connectionAdress);

            m_client = new ChatClient(channel);
        }

        public async void SendMessage(string content)
        {
            await m_client.SendAsync(
                 new ChatMessage
                 {
                     Content = content,
                     SenderName = m_name,
                     When = DateTime.Now.ToString()
                 }
                 );
        }

        public IObservable<ChatMessage> GetChatMessages()
        {
            byte[] encodingBytes = System.Text.Encoding.Unicode.GetBytes(m_name);
            string encodedName = Convert.ToBase64String(encodingBytes);

            Metadata headers = new Metadata();
            headers.Add("userName", encodedName);

            var call = m_client.Subscribe(headers);

            return call.ResponseStream.ReadAllAsync()
                .ToObservable()
                .Finally(() => call.Dispose());
        }

    }
}
