﻿namespace Client.Models
{
    class FormattedString
    {
        public FormattedString(string word)
        {
            this.Word = word;
            Bold = false;
            Italic = false;
            UnderLine = false;
            Cut = false;
        }

        public string Word { get; set; }

        public bool Bold { get; set; }

        public bool Italic { get; set; }

        public bool UnderLine { get; set; }

        public bool Cut { get; set; }
    }
}
