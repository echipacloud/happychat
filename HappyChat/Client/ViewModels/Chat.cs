﻿using Client.Models;
using Microsoft.VisualStudio.PlatformUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Markup.Localizer;

namespace HappyChatClient
{
    class Chat
    {
        private User m_user = new User();

        private readonly object m_chatHistoryLockObject = new object();
        public ObservableCollection<Object> ChatHistory { get; } = new ObservableCollection<Object>();
        public DelegateCommand<String> WriteCommand { get; }
        public Chat()
        {
            WriteCommand = new DelegateCommand<string>(m_user.SendMessage);
            BindingOperations.EnableCollectionSynchronization(ChatHistory, m_chatHistoryLockObject);
            StartReadingChat();
        }
        public Chat(String name)
        {
            m_user = new User(name);
            WriteCommand = new DelegateCommand<string>(m_user.SendMessage);
            BindingOperations.EnableCollectionSynchronization(ChatHistory, m_chatHistoryLockObject);
            StartReadingChat();
        }

        private void StartReadingChat()
        {
            var disposable = m_user.GetChatMessages()
                            .Subscribe((x) =>
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                var test = new TextBlock();
                                test.Inlines.Add(x.When);
                                var converter = new System.Windows.Media.BrushConverter();
                                var brush = (System.Windows.Media.Brush)converter.ConvertFromString("#FFFFFF00");

                                test.Inlines.Add(new Run(" " + x.SenderName + " ")
                                {
                                    Background = brush,
                                    Foreground = (System.Windows.Media.Brush)converter.ConvertFromString("#FF0000ff")
                                });


                                char[] formattingChars = { '*', '_', '~', '`' };
                                string[] words = x.Content.Split(' ');

                                Stack<Tuple<char, int, int>> stack = new Stack<Tuple<char, int, int>>();
                                List<FormattedString> formattedStrings = words.Select(x => new FormattedString(x)).ToList();

                                for (int i = 0; i < formattedStrings.Count(); i++)
                                {
                                    if (formattedStrings[i].Word.Length > 1)
                                    {
                                        int aux = formattedStrings[i].Word.Length - 1;
                                        int nrFormattingChars = formattedStrings[i].Word.Where(x => formattingChars.Contains(x)).Count();
                                        bool endStartFormatting = false;
                                        if (nrFormattingChars == formattedStrings[i].Word.Count())
                                        {
                                            endStartFormatting = true;
                                        }

                                        if ( endStartFormatting == false)
                                        {
                                            for (int j = 0; j < formattedStrings[i].Word.Length; j++)
                                            {
                                                char letter = formattedStrings[i].Word[j];
                                                if (formattingChars.Contains(letter) && endStartFormatting == false)
                                                {
                                                    stack.Push(new Tuple<char, int, int>(letter, i, j));
                                                }
                                                else
                                                {
                                                    endStartFormatting = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (endStartFormatting == true)
                                        {
                                            for (int j = formattedStrings[i].Word.Length - 1; j >= 0; j--)
                                            {
                                                char letter = formattedStrings[i].Word[j];
                                                if (!formattingChars.Contains(letter))
                                                {
                                                    break;
                                                }
                                                aux = j;
                                            }
                                        }




                                        while (stack.Count != 0 && formattingChars.Contains(formattedStrings[i].Word[aux]) && !(formattedStrings[i].Word.Distinct().Count() == 1))
                                        {
                                            var stackContainsChar = stack.Where(tuple => tuple.Item1 == formattedStrings[i].Word[aux]);
                                            if (stackContainsChar.Count() > 0)
                                            {
                                                while (stack.Peek().Item1 != formattedStrings[i].Word[aux])
                                                {
                                                    stack.Pop();
                                                    if (stack.Count == 0)
                                                    {
                                                        return;
                                                    }
                                                }
                                                char stackFormattingChar = stack.Peek().Item1;


                                                formattedStrings[stack.Peek().Item2].Word = formattedStrings[stack.Peek().Item2].Word.Substring(1, formattedStrings[stack.Peek().Item2].Word.Length - 1);
                                                if (formattedStrings[stack.Peek().Item2].Word == formattedStrings[i].Word)
                                                    aux--;
                                                formattedStrings[i].Word = formattedStrings[i].Word.Substring(0, aux) + formattedStrings[i].Word.Substring(aux + 1);
                                                if (/*formattedStrings[stack.Peek().Item2].Word == formattedStrings[i].Word &&*/ aux> formattedStrings[i].Word.Length-1)
                                                    aux--;
                                                for (int j = stack.Peek().Item2; j <= i; j++)
                                                {
                                                    if (stackFormattingChar == formattingChars[0])
                                                    {
                                                        formattedStrings[j].Bold = true;
                                                    }
                                                    if (stackFormattingChar == formattingChars[1])
                                                    {
                                                        formattedStrings[j].Italic = true;
                                                    }
                                                    if (stackFormattingChar == formattingChars[2])
                                                    {
                                                        formattedStrings[j].Cut = true;
                                                    }
                                                    if (stackFormattingChar == formattingChars[3])
                                                    {
                                                        formattedStrings[j].UnderLine = true;
                                                    }

                                                }
                                                stack.Pop();
                                                //i--;
                                            } else { break; }
                                        }
                                    }

                                }

                                foreach (FormattedString formattedString in formattedStrings)
                                {
                                    dynamic dynamic = new Run(formattedString.Word + " ");
                                    if (formattedString.Bold)
                                    {
                                        dynamic = new Bold(dynamic);
                                    }
                                    if (formattedString.Italic)
                                    {
                                        dynamic = new Italic(dynamic);
                                    }
                                    if (formattedString.UnderLine)
                                    {
                                        dynamic = new Underline(dynamic);
                                    }
                                    if (formattedString.Cut)
                                    {
                                        dynamic.TextDecorations = TextDecorations.Strikethrough;
                                    }

                                    test.Inlines.Add(dynamic);
                                }
                                ChatHistory.Add(test);
                            }));





            App.Current.Exit += (_, __) => { disposable.Dispose(); };

        }



    }
}
