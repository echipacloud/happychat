﻿using Client;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace HappyChatClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            Client.Window1 window = new Window1();
            window.ShowDialog();

            InitializeComponent();
            if (window.Name() != String.Empty)
                DataContext = new Chat(window.Name());
            else
                DataContext = new Chat();
        }

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var width = e.NewSize.Width;
            var height = e.NewSize.Height;

            var marg = chatview.Margin;
            marg.Left = 0;
            marg.Top = 0;
            chatview.Margin = marg;
            chatview.Width = width;

            marg.Top = chatview.Height = height - 25;
            chatmesaje.Margin = marg;
            marg.Left = chatmesaje.Width = width - 80;
            chatbutton.Margin = marg;
        }

        private void chatbutton_Click(object sender, RoutedEventArgs e)
        {
            if (chatmesaje.Text != String.Empty)
                (DataContext as Chat).WriteCommand.Execute(chatmesaje.Text);
            chatmesaje.Text = String.Empty;
        }

        private void BodyInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (chatmesaje.Text != String.Empty)
                    (DataContext as Chat).WriteCommand.Execute(chatmesaje.Text);
                chatmesaje.Text = String.Empty;
            }
        }

        private void BodyInput_Loaded(object sender, RoutedEventArgs e)
        {
            chatmesaje.Focus();
        }

        private void chatview_Loaded(object sender, RoutedEventArgs e)
        {
            var listBox = (ListBox)sender;

            var scrollViewer = FindScrollViewer(listBox);

            if (scrollViewer != null)
            {
                scrollViewer.ScrollChanged += (o, args) =>
                {
                    if (args.ExtentHeightChange > 0)
                        scrollViewer.ScrollToBottom();
                };
            }
        }

        private static ScrollViewer FindScrollViewer(DependencyObject root)
        {
            var queue = new Queue<DependencyObject>(new[] { root });

            do
            {
                var item = queue.Dequeue();

                if (item is ScrollViewer)
                    return (ScrollViewer)item;

                for (var i = 0; i < VisualTreeHelper.GetChildrenCount(item); i++)
                    queue.Enqueue(VisualTreeHelper.GetChild(item, i));
            } while (queue.Count > 0);

            return null;
        }

    }
}
