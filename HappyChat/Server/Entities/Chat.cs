﻿using Common.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

namespace Server.Entities
{
    public class Chat
    {
        private event Action<ChatMessage> addedMessage;
        private List<ChatMessage> history = new List<ChatMessage>();
        private const int MAX_HISTORY_MESSAGES = 1000;
        public void Add(ChatMessage chatMessage)
        {
            addedMessage?.Invoke(chatMessage);
            history.Add(chatMessage);

            if (history.Count() > MAX_HISTORY_MESSAGES)
            {
                history.RemoveAt(0);
            }
        }

        public IObservable<ChatMessage> GetChatMessages()
        {
            var newMessages = Observable.FromEvent<ChatMessage>((x) => addedMessage += x, (x) => addedMessage -= x);
            var oldMessages = history.ToObservable();

            return oldMessages.Concat(newMessages);
        }
    }
}
