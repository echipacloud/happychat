﻿using Common.Protos;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace HappyChat.Services
{
    public class ChatService : Chat.ChatBase
    {
        private Server.Entities.Chat m_chat;

        private readonly ILogger<ChatService> m_logger;
        public ChatService(ILogger<ChatService> logger, Server.Entities.Chat chat)
        {
            m_logger = logger;
            m_chat = chat;
        }

        public override async Task Subscribe(IAsyncStreamReader<Empty> requestStream, IServerStreamWriter<ChatMessage> responseStream, ServerCallContext context)
        {
            HttpContext httpContext = context.GetHttpContext();

            string userName = "anon";

            if(context.RequestHeaders.Count > 1)
            {
                userName = context.RequestHeaders.ElementAt(1).Value;
            }

            m_logger.LogInformation($"User" +
                $" {userName}" +
                $" with connection id {httpContext.Connection.Id} subscribes.");

            try
            {
                await m_chat.GetChatMessages()
                    .ToAsyncEnumerable()
                    .ForEachAwaitAsync(async (x) => await responseStream.WriteAsync(x), context.CancellationToken)
                    .ConfigureAwait(false);
            }
            catch (TaskCanceledException)
            {
                m_logger.LogInformation($"User" +
               $" {userName}" +
               $" with connection id {httpContext.Connection.Id} unsubscribes.");
            }
        }

        public override Task<Empty> Send(ChatMessage request, ServerCallContext context)
        {
            m_chat.Add(request);

            return Task.FromResult(new Empty());
        }
    }
}
